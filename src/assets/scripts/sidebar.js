const banner = $('#banner');
const about = $('#about');
const advisors = $('#advisors');
const portfolio = $('#portfolio');

let bannerPos = banner.offset().top + banner.innerHeight();
let aboutPos = about.offset().top + about.innerHeight();
let portfolioPos = portfolio.offset().top + portfolio.innerHeight();
let advisorsPos = advisors.offset().top + advisors.innerHeight();

const scrollItem = $('.scroll_item');
let scroll = $(window).scrollTop();

$(window).on('scroll', () => {
  scroll = $(window).scrollTop();
  console.log(scroll);
  if($(window).width() < 1500){
    if (scroll < bannerPos) {
      scrollItem.removeClass('scroll_item_active');
      $('#first').addClass('scroll_item_active');
    }
    if (scroll > bannerPos && scroll < aboutPos - 100) {
      scrollItem.removeClass('scroll_item_active');
      $('#second').addClass('scroll_item_active');
    }
    if (scroll > aboutPos && scroll < portfolioPos -100) {
      scrollItem.removeClass('scroll_item_active');
      $('#third').addClass('scroll_item_active');
    }
    if (scroll > portfolioPos && scroll < advisorsPos - 300 ) {
      scrollItem.removeClass('scroll_item_active');
      $('#fourth').addClass('scroll_item_active');
    }
    if (scroll > advisorsPos - 300 ) {
      scrollItem.removeClass('scroll_item_active');
      $('#fifth').addClass('scroll_item_active');
    }
  }
  if($(window).width() >= 1500){
    if (scroll < bannerPos) {
      scrollItem.removeClass('scroll_item_active');
      $('#first').addClass('scroll_item_active');
    }
    if (scroll > bannerPos && scroll < aboutPos) {
      scrollItem.removeClass('scroll_item_active');
      $('#second').addClass('scroll_item_active');
    }
    if (scroll > aboutPos && scroll < portfolioPos) {
      scrollItem.removeClass('scroll_item_active');
      $('#third').addClass('scroll_item_active');
    }
    if (scroll > portfolioPos && scroll < advisorsPos - 300 ) {
      scrollItem.removeClass('scroll_item_active');
      $('#fourth').addClass('scroll_item_active');
    }
    if (scroll > advisorsPos - 100 ) {
      scrollItem.removeClass('scroll_item_active');
      $('#fifth').addClass('scroll_item_active');
    }
  }
});